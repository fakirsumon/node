'use strict';
/* eslint-disable no-console */
const Database      = require('./database');
const sqlStatements = require('./sqlStatements');

const giftDB = new Database({
  host        : 'localhost',
  port        : 3306,
  user        : 'saku',
  password    : 'secret',
  database    : 'productdatabase'
}, true);

const allSql = sqlStatements.getAllSql.join(' ');

giftDB.doQuery(allSql)
  .then(result => console.log(result))
  .catch(err => console.log(err.message));
